package com.redhat.demo.amq;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class AMQHelper {

	public AMQHelper() {
		super();
	}

	void publishMessageToTestQueue(String jsonMessage) {
		if (!jsonMessage.isEmpty())
		{
			try {
				ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
	
				// Create a Connection
				Connection connection = connectionFactory.createConnection();
				connection.start();
	
				// Create a Session
				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	
				// Create the destination (Topic or Queue)
				Destination destination = session.createQueue("TEST.QUEUE");
	
				// Create a MessageProducer from the Session to the Topic or Queue
				MessageProducer producer = session.createProducer(destination);
				producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				
				// Create the message
				TextMessage message = session.createTextMessage(jsonMessage);
				 
	            // Tell the producer to send the message
	            producer.send(message);
	            System.out.println("Sent message: "+ message.hashCode() + " : " + Thread.currentThread().getName());
	
	            // Clean up
	            session.close();
	            connection.close();
			} catch (Exception e) {
				System.out.println("Caught: " + e);
				e.printStackTrace();
			}
		}
	}
	
	void test(String jsonMessage) {
		System.out.println("Would be printing this message out: " + jsonMessage);
	}
}
