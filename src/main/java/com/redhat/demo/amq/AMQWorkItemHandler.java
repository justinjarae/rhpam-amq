package com.redhat.demo.amq;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jbpm.process.workitem.core.AbstractLogOrThrowWorkItemHandler;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AMQWorkItemHandler  extends AbstractLogOrThrowWorkItemHandler {

	private static final Logger logger = LoggerFactory.getLogger(AMQWorkItemHandler.class);

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		String jsonMessage = (String) workItem.getParameter("JsonMessage");
		String queueName = (String) workItem.getParameter("QueueName");
		String amqAddress = (String) workItem.getParameter("AMQAddress");
		String amqPort = (String) workItem.getParameter("AMQPort");
		Boolean result = null;
		Map<String, Object> results = new HashMap<String, Object>();
		result = true;
		
		if (amqAddress == null || !amqAddress.isEmpty()) {
			amqAddress = "localhost";
		}
		
		if (amqPort == null || !amqPort.isEmpty()) {
			amqPort = "61616";
		}
		
		logger.info("HEY! THIS WORKED!");
		
		if (true && !jsonMessage.isEmpty() && !queueName.isEmpty())
		{
			try {
				ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://" + amqAddress + ":" + amqPort);
				logger.info("Created connection factory");
	
				// Create a Connection
				Connection connection = connectionFactory.createConnection();
				connection.start();
				logger.info("Started connection");
	
				// Create a Session
				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	
				// Create the destination (Topic or Queue)
				Destination destination = session.createQueue(queueName);
	
				// Create a MessageProducer from the Session to the Topic or Queue
				MessageProducer producer = session.createProducer(destination);
				producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				
				// Create the message
				TextMessage message = session.createTextMessage(jsonMessage);
				 
	            // Tell the producer to send the message
	            producer.send(message);
	            System.out.println("Sent message: "+ message.hashCode() + " : " + Thread.currentThread().getName());
	
	            // Clean up
	            session.close();
	            connection.close();
	            result = true;
			} catch (Exception e) {
				System.out.println("Caught: " + e);
				e.printStackTrace();
				result = false;
			}
		}
		

		results.put("Result", result);		
		manager.completeWorkItem(workItem.getId(), results);
	}

	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		// Do nothing
	}
}
